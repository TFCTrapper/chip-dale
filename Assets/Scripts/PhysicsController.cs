using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicsController : MonoBehaviour
{
    public Vector2 velocity { get; set; }
    public float groundY { get => _groundY; private set => _groundY = value; }
    public Action OnLanded;

    [SerializeField] private float _gravitationalAcceleration;
    [SerializeField] private float _bounciness;
    [SerializeField] private float _groundY;
    [SerializeField] private float _minVelocityToBounce;

    private void Update()
    {
        if (transform.position.y > _groundY)
        {
            float yOffset = velocity.y * Time.deltaTime + _gravitationalAcceleration * Time.deltaTime * Time.deltaTime / 2f;
            float velocityY = velocity.y + _gravitationalAcceleration * Time.deltaTime;
            velocity = new Vector2(velocity.x, velocityY);
            transform.Translate(Vector3.up * yOffset);
        }

        if (transform.position.y < _groundY)
        {
            transform.position = new Vector3(transform.position.x, _groundY, transform.position.z);
            if (Mathf.Abs(velocity.y) > _minVelocityToBounce)
            {
                Bounce();
            }
            else
            {
                velocity = Vector2.zero;
                OnLanded?.Invoke();
            }
        }
    }

    private void Bounce()
    {
        float velocityY = velocity.y * -_bounciness;
        velocity = new Vector2(velocity.x, velocityY);
        transform.Translate(Vector3.up * 0.05f);
    }
}
