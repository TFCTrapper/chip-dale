using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    [SerializeField] private GameObject _mainPanel;
    [SerializeField] private GameObject _menuPanel;
    [SerializeField] private GameObject _resultPanel;

    [SerializeField] private Text _resultText;
    [SerializeField] private Button _continueButton;
    [SerializeField] private Button _restartButton;
    [SerializeField] private Button _exitButton;

    [SerializeField] private Toggle _musicToggle;

    [SerializeField] private Player _player;
    [SerializeField] private Ship _ship;
    [SerializeField] private MusicManager _musicManager;

    [SerializeField] private string _successText;
    [SerializeField] private string _failureText;

    private void Awake()
    {
        Time.timeScale = 1f;

        _player = FindObjectOfType<Player>();
        _ship = FindObjectOfType<Ship>();

        _player.OnDestroyed += OnPlayerDestroyed;
        _ship.OnDestroyed += OnShipDestroyed;

        _continueButton.onClick.AddListener(OnContinueButtonClick);
        _restartButton.onClick.AddListener(OnRestartButtonClick);
        _exitButton.onClick.AddListener(OnExitButtonClick);

        _musicToggle.onValueChanged.AddListener(OnMusicToggleValueChanged);
    }

    private void OnDestroy()
    {
        _player.OnDestroyed -= OnPlayerDestroyed;
        _ship.OnDestroyed -= OnShipDestroyed;

        _continueButton.onClick.RemoveListener(OnContinueButtonClick);
        _restartButton.onClick.RemoveListener(OnRestartButtonClick);
        _exitButton.onClick.RemoveListener(OnExitButtonClick);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && !_resultPanel.activeInHierarchy)
        {
            Time.timeScale = _mainPanel.activeInHierarchy ? 0f : 1f;

            _mainPanel.SetActive(!_mainPanel.activeInHierarchy);
            _menuPanel.SetActive(!_menuPanel.activeInHierarchy);
        }
    }

    private void OnPlayerDestroyed()
    {
        ShowResult(false);
    }

    private void OnShipDestroyed()
    {
        ShowResult(true);
    }

    private void OnContinueButtonClick()
    {
        Time.timeScale = 1f;
        _mainPanel.SetActive(true);
        _menuPanel.SetActive(false);
    }

    private void OnRestartButtonClick()
    {
        SceneManager.LoadScene(0);
    }

    private void OnExitButtonClick()
    {
        Application.Quit();
    }

    private void ShowResult(bool success)
    {
        Time.timeScale = 0f;

        _resultPanel.SetActive(true);
        _menuPanel.SetActive(true);
        _continueButton.gameObject.SetActive(false);

        _resultText.text = success ? _successText : _failureText;
    }

    private void OnMusicToggleValueChanged(bool value)
    {
        _musicManager.audioSource.mute = !value;
    }
}