using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HPPanel : MonoBehaviour
{
    [SerializeField] private GameObject _heartPrefab;
    [SerializeField] private Player _player;

    private List<GameObject> _hearts = new List<GameObject>();

    private void Awake()
    {
        _player.OnDamaged += OnPlayerDamaged;

        for (int i = 0; i < _player.hp; i++)
        {
            GameObject heart = Instantiate(_heartPrefab);
            heart.transform.SetParent(transform);
            _hearts.Add(heart);
        }
    }

    private void OnPlayerDamaged(int hp)
    {
        Destroy(_hearts[_hearts.Count - 1]);
        _hearts.RemoveAt(_hearts.Count - 1);
    }
}
