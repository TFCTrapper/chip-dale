using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour, IDamagable
{
    public Action<int> OnDamaged;
    public Action OnDestroyed;

    public int hp { get => _hp; private set => _hp = value; }

    [Header("Movement")]
    [SerializeField] private float _speed;
    [SerializeField] private float _jumpFactor;
    [SerializeField] private float _damageJumpFactor;

    [Header("HP")]
    [SerializeField] private int _hp;
    [SerializeField] private DamagableState _state;

    [Header("CooldownAnimation")]
    [SerializeField] private float timeToSwitchRendererEnabled;
    [SerializeField] private float rendererTimeDecreaseFactor;
    [SerializeField] private float minTimeToSwitchRendererEnabled;

    [Header("Picking Objects")]
    [SerializeField] private Transform slot;
    [SerializeField] private GameObject pickedObject;
    [SerializeField] private float maxDistanceToPick;
    [SerializeField] private GameObject ball;

    private Animator _animator;
    private SpriteRenderer _spriteRenderer;
    private PhysicsController _physicsController;

    public void GetDamage()
    {
        if (_state == DamagableState.Default)
        {
            _hp--;
            OnDamaged?.Invoke(_hp);
            if (_hp <= 0)
            {
                OnDestroyed?.Invoke();
                Destroy(gameObject);
            }

            _physicsController.velocity = Vector2.up * _damageJumpFactor;
            transform.Translate(Vector2.up * 0.01f);

            _state = DamagableState.Cooldown;
            StartCoroutine(CooldownCoroutine());
        }
    }

    private void Awake()
    {
        _animator = GetComponent<Animator>();
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _physicsController = GetComponent<PhysicsController>();
    }

    private void Update()
    {
        float _horizontalAxis = Input.GetAxisRaw("Horizontal");

        _animator.SetFloat("HorizontalAxisAbs", Mathf.Abs(_horizontalAxis));
        _animator.SetFloat("PositionY", transform.position.y - _physicsController.groundY);
        transform.Translate(Vector3.right * _horizontalAxis * _speed * Time.deltaTime);

        if (_horizontalAxis > 0 && _spriteRenderer.flipX)
        {
            _spriteRenderer.flipX = false;
        }
        if (_horizontalAxis < 0 && !_spriteRenderer.flipX)
        {
            _spriteRenderer.flipX = true;
        }
        
        if (_physicsController.velocity.y == 0f)
        {
            _physicsController.velocity = Vector2.up * Input.GetAxisRaw("Jump") * _jumpFactor;
            if (_physicsController.velocity.y > 0f)
            {
                transform.Translate(Vector2.up * 0.01f);
            }
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            if (pickedObject != null)
            {
                Throw();
            }
            else
            {
                if (
                    (ball.transform.position - transform.position).sqrMagnitude <= maxDistanceToPick * maxDistanceToPick
                   )
                {
                    Pick(ball);
                }
            }
        }
    }

    private void Pick(GameObject objectToPick)
    {
        objectToPick.transform.SetParent(slot);
        objectToPick.transform.localPosition = Vector3.zero;
        pickedObject = objectToPick;
        pickedObject.GetComponent<PhysicsController>().enabled = false;
        _animator.SetBool("HasObject", true);
    }

    private void Throw()
    {
        pickedObject.transform.SetParent(null);
        pickedObject.GetComponent<PhysicsController>().enabled = true;
        pickedObject.GetComponent<PhysicsController>().velocity = Vector3.up * 10f;
        pickedObject = null;
        _animator.SetBool("HasObject", false);
    }

    private IEnumerator CooldownCoroutine()
    {
        float time = timeToSwitchRendererEnabled;
        while (time > minTimeToSwitchRendererEnabled)
        {
            yield return new WaitForSeconds(time);
            time *= rendererTimeDecreaseFactor;
            _spriteRenderer.enabled = !_spriteRenderer.enabled;
        }
        _spriteRenderer.enabled = true;
        _state = DamagableState.Default;
    }
}
