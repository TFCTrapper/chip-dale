using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ship : MonoBehaviour, IDamagable
{
    public Action OnDamaged;
    public Action OnDestroyed;

    [Header("Movement")]
    [SerializeField] private float _speed;
    [SerializeField] private Vector2 _xBounds; //min&max X position
    [SerializeField] private Vector2 _yBounds;

    [Header("Aliens Spawn")]
    [SerializeField] private AliensManager _aliensManager;
    [SerializeField] private float _spawnAttemptPeriod;
    [SerializeField] private float _spawnProbability;

    [Header("HP")]
    [SerializeField] private int _hp;
    [SerializeField] private DamagableState _state;

    [Header("CooldownAnimation")]
    [SerializeField] private float timeToSwitchRendererEnabled;
    [SerializeField] private float rendererTimeDecreaseFactor;
    [SerializeField] private float minTimeToSwitchRendererEnabled;

    private int _directionFactor = 1;
    private List<SpriteRenderer> _spriteRenderers;

    public void GetDamage()
    {
        if (_state == DamagableState.Default)
        {
            _hp--;
            OnDamaged?.Invoke();
            if (_hp <= 0)
            {
                OnDestroyed?.Invoke();
                Destroy(gameObject);
            }
            _state = DamagableState.Cooldown;
            StartCoroutine(CooldownCoroutine());
        }
    }

    private void Awake()
    {
        _spriteRenderers = new List<SpriteRenderer>(GetComponentsInChildren<SpriteRenderer>());
        UpdateY();
        StartCoroutine(SpawnCoroutine());
    }

    private void Update()
    {
        transform.Translate(Vector3.right * _directionFactor * _speed * Time.deltaTime);
        if (transform.position.x <= _xBounds.x || transform.position.x >= _xBounds.y)
        {
            Debug.Log("Ship flip");
            UpdateY();
            transform.localScale = new Vector3(transform.localScale.x * -1f, transform.localScale.y, transform.localScale.z);
            _directionFactor *= -1;
            transform.Translate(Vector3.right * _directionFactor * _speed * Time.deltaTime * 2f);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Equals("Ball") && collision.transform.parent == null)
        {
            GetDamage();
        }
    }

    private void UpdateY()
    {
        float y = UnityEngine.Random.Range(_yBounds.x, _yBounds.y);
        transform.position = new Vector3(transform.position.x, y, transform.position.z);
    }

    private void CreateAlien()
    {
        _aliensManager.Pull(transform.position, Quaternion.identity);
    }

    private IEnumerator CooldownCoroutine()
    {
        float time = timeToSwitchRendererEnabled;
        while (time > minTimeToSwitchRendererEnabled)
        {
            SetSpriteRenderersEnabled(!_spriteRenderers[0].enabled);
            yield return new WaitForSeconds(time);
            time *= rendererTimeDecreaseFactor;
        }
        SetSpriteRenderersEnabled(true);
        _state = DamagableState.Default;
    }

    private IEnumerator SpawnCoroutine()
    {
        while (gameObject != null)
        {
            yield return new WaitForSeconds(_spawnAttemptPeriod);
            float random = UnityEngine.Random.Range(0f, 1f);
            if (random <= _spawnProbability)
            {
                CreateAlien();
            }
        }
    }

    private void SetSpriteRenderersEnabled(bool value)
    {
        foreach (var spriteRenderer in _spriteRenderers)
        {
            spriteRenderer.enabled = value;
        }
    }
}
