using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDamagable
{
    void GetDamage();
}

public interface IPool
{
    void Push(GameObject gameObject);
    GameObject Pull(Vector3 position, Quaternion rotation);
}