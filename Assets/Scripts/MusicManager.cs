using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour
{
    [Serializable]
    private class Music
    {
        public AudioClip audioClip;
        public bool loop;
    }

    public AudioSource audioSource { get => _audioSource; private set => _audioSource = value; }

    [SerializeField] private List<Music> _music;
    [SerializeField] private AudioSource _audioSource;
    [SerializeField] private int _currentMusicIndex;

    [SerializeField] private Ship _ship;

    private Action OnMusicEnded;

    private void Awake()
    {
        _currentMusicIndex = 0;
        _audioSource.clip = _music[0].audioClip;
        _audioSource.loop = _music[0].loop;
        _audioSource.Play();
        OnMusicEnded += OnCurrentMusicEnded;
        _ship.OnDestroyed += OnCurrentMusicEnded;
    }

    private void OnDestroy()
    {
        OnMusicEnded -= OnCurrentMusicEnded;
        _ship.OnDestroyed -= OnCurrentMusicEnded;
    }

    private void Update()
    {
        if (!_audioSource.isPlaying)
        {
            OnMusicEnded?.Invoke();
        }
    }

    private void OnCurrentMusicEnded()
    {
        _currentMusicIndex++;
        if (_music.Count > _currentMusicIndex)
        {
            _audioSource.clip = _music[_currentMusicIndex].audioClip;
            _audioSource.loop = _music[_currentMusicIndex].loop;
            _audioSource.Play();
        }
    }
}
