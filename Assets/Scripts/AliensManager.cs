using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AliensManager : MonoBehaviour, IPool
{
    [SerializeField] private GameObject _alienPrefab;
    [SerializeField] private List<GameObject> _aliens;

    public void Push(GameObject alien)
    {
        alien.SetActive(false);
    }

    public GameObject Pull(Vector3 position, Quaternion rotation)
    {
        GameObject alien = _aliens.Where(a => !a.activeInHierarchy).FirstOrDefault();

        if (alien == null)
        {
            alien = Instantiate(_alienPrefab, position, rotation);
            _aliens.Add(alien);
            alien.transform.SetParent(transform);
        }
        else
        {
            alien.transform.position = position;
            alien.transform.rotation = rotation;
            alien.SetActive(true);
        }

        alien.GetComponent<Alien>().Initialize(this);
        return alien;
    }
}
