using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Alien : MonoBehaviour
{
    private enum State
    {
        Idle = 0,
        Running
    }

    [SerializeField] private Vector2 _initialVelocity;
    [SerializeField] private float _speed;
    [SerializeField] private State _state;
    [SerializeField] private Player _player;
    [SerializeField] private Vector2 _xBounds; //min&max X position

    [SerializeField] private AliensManager _aliensManager;

    private PhysicsController _physicsController;
    private int _directionFactor;

    public void Initialize(AliensManager aliensManager)
    {
        _aliensManager = aliensManager;

        _player = FindObjectOfType<Player>();
        _physicsController = GetComponent<PhysicsController>();
        _physicsController.OnLanded += OnLanded;
        _physicsController.velocity = _initialVelocity;
        _state = State.Idle;
    }

    private void OnDestroy()
    {
        _physicsController.OnLanded -= OnLanded;
    }

    private void Update()
    {
        if (_state == State.Running)
        {
            transform.Translate(Vector3.right * _directionFactor * _speed * Time.deltaTime);
            if (transform.position.x < _xBounds.x || transform.position.x > _xBounds.y)
            {
                _aliensManager.Push(gameObject);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Player player = collision.gameObject.GetComponent<Player>();
        if (player != null)
        {
            player.GetDamage();
        }
    }

    private void OnLanded()
    {
        _state = State.Running;
        _directionFactor = (_player != null && _player.transform.position.x > transform.position.x) ? 1 : -1;
        GetComponent<SpriteRenderer>().flipX = _directionFactor < 0;
    }
}