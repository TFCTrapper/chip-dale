using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private Animator _animator;
    [SerializeField] private Ship _ship;
    [SerializeField] private float _shakeTime;

    private void Awake()
    {
        _animator = GetComponent<Animator>();
        _ship.OnDamaged += OnShipDamaged;
    }

    private void OnDestroy()
    {
        _ship.OnDamaged -= OnShipDamaged;
    }

    private void OnShipDamaged()
    {
        Shake();
    }

    private void Shake()
    {
        _animator.SetBool("IsShaking", true);
        StartCoroutine(ShakeCoroutine());
    }

    private IEnumerator ShakeCoroutine()
    {
        yield return new WaitForSeconds(_shakeTime);
        _animator.SetBool("IsShaking", false);
    }
}